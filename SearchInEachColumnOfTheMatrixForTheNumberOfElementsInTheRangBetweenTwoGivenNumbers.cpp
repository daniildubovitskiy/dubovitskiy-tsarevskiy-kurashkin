#include <iostream>
#include <ctime>
#include <Windows.h>
#include <iomanip>
 
using namespace std;
const int MatrixSize = 14, MatrixInterval = 10;
void FillMatrix(int matrix[MatrixSize][MatrixSize]);
void PrintMatrix(int matrix[MatrixSize][MatrixSize]);
void PrintArray(int array[MatrixSize]);
 
int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int MatrixMinElement{}, MatrixMaxElement{}, num[MatrixSize]{}, sum{}, matrix[MatrixSize][MatrixSize]{};
    cout << "Исходная матрица: " << endl;
    FillMatrix(matrix);
    PrintMatrix(matrix);
 
    //Задание 1
    cout << "\nВведите начало диапазона: ";
    cin >> MatrixMinElement;
 
    while ((MatrixMinElement < -MatrixInterval) || (MatrixMinElement > MatrixInterval - 1)) {   //этот цикл нужен для проверки ввода
        cout << "Неверное значение! Повторите попытку: ";
        cin >> MatrixMinElement;
    }
 
    cout << "Введите конец диапазона: ";
    cin >> MatrixMaxElement;
 
    while ((MatrixMaxElement < -MatrixInterval + 1) || (MatrixMaxElement > MatrixInterval) || (MatrixMaxElement <= MatrixMinElement)) {     //цикл нужен для проверки ввода
        cout << "Неверное значение! Повторите попытку: ";
        cin >> MatrixMaxElement;
    }
 
    cout << "Количества элементов, входящих в данный диапазон, в каждом столбце равны: " << endl;
    for (int j = 0; j < MatrixSize; j++) {
        for (int i = 0; i < MatrixSize; i++) {
            if ((matrix[i][j] >= MatrixMinElement) && (matrix[i][j] <= MatrixMaxElement)) {
                num[j]++;
            }
        }
    }
    PrintArray(num);
    void FillMatrix(int matrix[MatrixSize][MatrixSize]) {
    srand(time(nullptr));
    for (int i = 0; i < MatrixSize; i++) {
        for (int j = 0; j < MatrixSize; j++) {
            matrix[i][j] = rand() % (2 * MatrixInterval + 1) - MatrixInterval;
        }
    }
}
 
void PrintMatrix(int matrix[MatrixSize][MatrixSize]) {
    for (int i = 0; i < MatrixSize; i++) {
        for (int j = 0; j < MatrixSize; j++) {
            cout << setw(4) << matrix[i][j];
        }
        cout << endl;
    }
}
 
void PrintArray(int array[MatrixSize]) {
    for (int i = 0; i < MatrixSize; i++) {
        cout << setw(4) << array[i];
    }
    cout << endl;
}