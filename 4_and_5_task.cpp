#include <iostream>
#include <ctime>
#include <Windows.h>
#include <iomanip>

using namespace std;
const int MatrixSize = 14, MatrixInterval = 10;
void FillMatrix(int matrix[MatrixSize][MatrixSize]);
void PrintMatrix(int matrix[MatrixSize][MatrixSize]);
void PrintArray(int array[MatrixSize]);

int main()
{
SetConsoleCP(1251);
SetConsoleOutputCP(1251);
int MatrixMinElement{}, MatrixMaxElement{}, num[MatrixSize]{}, sum{}, matrix[MatrixSize][MatrixSize]{};
//Задание 4
cout « "\nСуммы модулей элементов, расположенных после первого отрицательного элемента, равны: " « endl;
for (int i = 0; i < MatrixSize; i++) {
flag = false;
sum = 0;
for (int j = 0; j < MatrixSize; j++) {
if (flag == true) {
sum += abs(matrix[i][j]);
}
if (matrix[i][j] < 0) {
flag = true;
}
}
cout « i + 1 « ") " « sum « endl;
}
cout « endl;

//Задание 5
for (int i = 1; i <= MatrixSize / 2; i++) {
flag = true;
for (int j = 0; j < MatrixSize; j++) {
if (matrix[i - 1][j] != matrix[MatrixSize - i][j]) {
flag = false;
}
}
if (flag == true) {
cout « "Строки " « i « " и " « MatrixSize - i + 1 « " равны" « endl;
}
else {
cout « "Строки " « i « " и " « MatrixSize - i + 1 « " не равны" « endl;
}
}
return 0;
}