#include <iostream>
#include <ctime>
#include <Windows.h>
#include <iomanip>

using namespace std;
const int MatrixSize = 14, MatrixInterval = 10;
void FillMatrix(int matrix[MatrixSize][MatrixSize]);
void PrintMatrix(int matrix[MatrixSize][MatrixSize]);
void PrintArray(int array[MatrixSize]);

int main()
{
SetConsoleCP(1251);
SetConsoleOutputCP(1251);
int MatrixMinElement{}, MatrixMaxElement{}, num[MatrixSize]{}, sum{}, matrix[MatrixSize][MatrixSize]{};
/Задание2
bool flag{};
cout « "\nСредние арифметические значения в ненулевых строках равны: " « endl;
for (int i = 0; i < MatrixSize; i++) {
flag = false;
for (int j = 0; j < MatrixSize; j++) {
if (matrix[i][j] != 0) {
sum = sum + matrix[i][j];
flag = true;
}
}
if (flag == true) {
cout « i + 1 « ") " « sum / MatrixSize « endl;
}
}
cout « "\n";

//Задание 3
flag = true;
for (int i = 0; i < MatrixSize; i++) {
for (int j = 0; j <= i; j++) {
if ((matrix[i][j] > 0) && (flag == true)) {
cout « "Первый положительный элемент в выделенной области равен " « matrix[i][j];
flag = false;
}
}
}
cout « "\n";
}